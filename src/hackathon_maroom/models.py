from django.db import models
from django.contrib.auth.models import User


class Company(models.Model):
    name = models.CharField(max_length=256)

    class Meta:
        verbose_name_plural = "companies"

    def __str__(self):
        return f"{self.name} ({self.id})"


class UserExtension(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user.first_name} ({self.id})"


class ACLModel(models.TextChoices):
    # max_length = 6
    PUBLIC = "puball"
    PUBLIC_META = "pubmet"
    PUBLIC_NAME = "pubnam"
    HIDDEN = "hidden"


class Dataset(models.Model):
    company_creator = models.ForeignKey(Company, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=256)
    datahub_urn = models.CharField(max_length=256, unique=True)  # not sure, but its max_length's 128 actually
    default_ACL_model = models.CharField(max_length=6, choices=ACLModel.choices, default=ACLModel.HIDDEN)

    def __str__(self):
        return f"{self.name} ({self.id})"


class AccessControl(models.Model):
    for_dataset = models.ForeignKey(Dataset, on_delete=models.CASCADE)
    for_user = models.ForeignKey(UserExtension, blank=True, null=True, on_delete=models.CASCADE)
    for_company = models.ForeignKey(Company, blank=True, null=True, on_delete=models.CASCADE)
    default_ACL_model = models.CharField(max_length=6, choices=ACLModel.choices)

    class Meta:
        unique_together = [
            ["for_dataset", "for_user", "for_company"],
        ]
        indexes = [
            models.Index(fields=["for_dataset", "for_user"]),
            models.Index(fields=["for_dataset", "for_company"]),
        ]

    def __str__(self):
        connection = f"User: {self.for_user}" if self.for_company else f"Company: {self.for_company}"
        return f"{self.for_dataset} for {connection} ({self.id})"
