from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import UserExtension, Company, Dataset, AccessControl

admin.site.register(UserExtension)
admin.site.register(Company)
admin.site.register(Dataset)
admin.site.register(AccessControl)
