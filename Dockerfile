FROM python:3.8.6-slim

RUN apt-get update
RUN apt-get install -y \
        gcc \
        make \
        libffi-dev \
        musl-dev \
        libpq-dev

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PIP_DISABLE_PIP_VERSION_CHECK 1

WORKDIR /app

# install dependencies
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY src/hackathon_maroom /app/hackathon_maroom
COPY src/manage.py /app/manage.py

CMD ["gunicorn", "hackathon_maroom.wsgi:application", \
        "--bind", "0.0.0.0:8000", \
        "--timeout",  "30", \
        "--log-level", "debug", \
        "--access-logfile", "-", \
        "--error-logfile", "-"]
