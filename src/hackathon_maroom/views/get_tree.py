from rest_framework import renderers
from rest_framework.response import Response

from hackathon_maroom.gql.proxy import GQLProxyView


class GetDatasetTree(GQLProxyView):
    renderer_classes = (renderers.JSONRenderer,)
    query = """
query getBrowseResults($input: BrowseInput!) {
  browse(input: $input) {
    entities {
      urn
      type
      ... on Dataset {
        name
        origin
        description
        platform {
          name
          info {
            logoUrl
            __typename
          }
          __typename
        }
        ownership {
          ...ownershipFields
          __typename
        }
        globalTags {
          ...globalTagsFields
          __typename
        }
        glossaryTerms {
          ...glossaryTerms
          __typename
        }
        subTypes {
          typeNames
          __typename
        }
        __typename
      }
      ... on Dashboard {
        urn
        type
        tool
        dashboardId
        info {
          name
          description
          externalUrl
          access
          lastModified {
            time
            __typename
          }
          __typename
        }
        ownership {
          ...ownershipFields
          __typename
        }
        globalTags {
          ...globalTagsFields
          __typename
        }
        glossaryTerms {
          ...glossaryTerms
          __typename
        }
        __typename
      }
      ... on GlossaryTerm {
        name
        ownership {
          ...ownershipFields
          __typename
        }
        glossaryTermInfo {
          definition
          termSource
          sourceRef
          sourceUrl
          customProperties {
            key
            value
            __typename
          }
          __typename
        }
        __typename
      }
      ... on Chart {
        urn
        type
        tool
        chartId
        info {
          name
          description
          externalUrl
          type
          access
          lastModified {
            time
            __typename
          }
          __typename
        }
        ownership {
          ...ownershipFields
          __typename
        }
        globalTags {
          ...globalTagsFields
          __typename
        }
        glossaryTerms {
          ...glossaryTerms
          __typename
        }
        __typename
      }
      ... on DataFlow {
        urn
        type
        orchestrator
        flowId
        cluster
        info {
          name
          description
          project
          __typename
        }
        ownership {
          ...ownershipFields
          __typename
        }
        globalTags {
          ...globalTagsFields
          __typename
        }
        glossaryTerms {
          ...glossaryTerms
          __typename
        }
        __typename
      }
      ... on MLFeatureTable {
        urn
        type
        name
        description
        featureTableProperties {
          description
          mlFeatures {
            urn
            __typename
          }
          mlPrimaryKeys {
            urn
            __typename
          }
          __typename
        }
        ownership {
          ...ownershipFields
          __typename
        }
        platform {
          name
          info {
            logoUrl
            __typename
          }
          __typename
        }
        __typename
      }
      ... on MLModel {
        name
        origin
        description
        ownership {
          ...ownershipFields
          __typename
        }
        globalTags {
          ...globalTagsFields
          __typename
        }
        platform {
          name
          info {
            logoUrl
            __typename
          }
          __typename
        }
        __typename
      }
      ... on MLModelGroup {
        name
        origin
        description
        ownership {
          ...ownershipFields
          __typename
        }
        platform {
          name
          info {
            logoUrl
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
    groups {
      name
      count
      __typename
    }
    start
    count
    total
    metadata {
      path
      totalNumEntities
      __typename
    }
    __typename
  }
}

fragment ownershipFields on Ownership {
  owners {
    owner {
      ... on CorpUser {
        urn
        type
        username
        info {
          active
          displayName
          title
          email
          firstName
          lastName
          fullName
          __typename
        }
        editableInfo {
          pictureLink
          __typename
        }
        __typename
      }
      ... on CorpGroup {
        urn
        type
        name
        info {
          email
          admins {
            urn
            username
            info {
              active
              displayName
              title
              email
              firstName
              lastName
              fullName
              __typename
            }
            editableInfo {
              pictureLink
              teams
              skills
              __typename
            }
            __typename
          }
          members {
            urn
            username
            info {
              active
              displayName
              title
              email
              firstName
              lastName
              fullName
              __typename
            }
            editableInfo {
              pictureLink
              teams
              skills
              __typename
            }
            __typename
          }
          groups
          __typename
        }
        __typename
      }
      __typename
    }
    type
    __typename
  }
  lastModified {
    time
    __typename
  }
  __typename
}

fragment globalTagsFields on GlobalTags {
  tags {
    tag {
      urn
      name
      description
      __typename
    }
    __typename
  }
  __typename
}

fragment glossaryTerms on GlossaryTerms {
  terms {
    term {
      urn
      name
      __typename
    }
    __typename
  }
  __typename
}
    
    """

    variables = {
        "input": {
            "type": "DATASET",
            "path": [
                "prod",
                "bigquery",
                "bigquery-public-data",
                "covid19_geotab_mobility_impact"
            ],
            "start": 0,
            "count": 20,
            "filters": None
        }
    }

    def get(self, request):
        result = self.client(self.query, self.variables)

        return Response(result)
