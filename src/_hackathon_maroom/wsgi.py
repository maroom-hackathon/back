# import os
#
# import sentry_sdk
from django.core.wsgi import get_wsgi_application
# from sentry_sdk.integrations.django import DjangoIntegration
#
# sentry_sdk.init(
#     dsn="https://d53cc86be1e14bf8a79fab41e1cafe22@o469342.ingest.sentry.io/5578611",
#     integrations=[DjangoIntegration()],
#     environment=os.environ.get('SENTRY_ENV', 'local'),
#     traces_sample_rate=1.0,
#     # If you wish to associate users to errors (assuming you are using
#     # django.contrib.auth) you may enable sending PII data.
#     send_default_pii=True,
# )
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hackathon_maroom.settings")

application = get_wsgi_application()
