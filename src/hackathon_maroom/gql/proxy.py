from rest_framework import views

from sgqlc.endpoint.http import HTTPEndpoint


def get_gql_client():
    transport = HTTPEndpoint(
        "https://datahub.hackathon-maroom.tech/api/graphql",
        base_headers={
            'Cookie': 'bid=399fd716-3af6-466c-bc27-85685fbb137c; PLAY_SESSION=669be67ce71855799441548a73a0d8a4293d0b89-actor=urn%3Ali%3Acorpuser%3Adatahub; actor=urn:li:corpuser:datahub'
        }
    )
    return transport


class GQLProxyView(views.APIView):
    client = get_gql_client()

    @classmethod
    def query_gql(cls, query_txt: str, variables=None):
        return cls.client(query_txt, variables=variables)
